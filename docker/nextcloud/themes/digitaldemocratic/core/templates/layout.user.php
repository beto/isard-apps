<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="<?php p($_['language']); ?>" data-locale="<?php p($_['locale']); ?>" >
	<head data-user="<?php p($_['user_uid']); ?>" data-user-displayname="<?php p($_['user_displayname']); ?>" data-requesttoken="<?php p($_['requesttoken']); ?>">
		<meta charset="utf-8">
		<title>
			<?php
				p(!empty($_['application'])?$_['application'].' - ':'');
				p($theme->getTitle());
			?>
		</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
		<?php if ($theme->getiTunesAppId() !== '') { ?>
		<meta name="apple-itunes-app" content="app-id=<?php p($theme->getiTunesAppId()); ?>">
		<?php } ?>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-title" content="<?php p((!empty($_['application']) && $_['appid'] != 'files')? $_['application']:$theme->getTitle()); ?>">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="theme-color" content="<?php p($theme->getColorPrimary()); ?>">
		<link rel="icon" href="<?php print_unescaped(image_path($_['appid'], 'favicon.ico')); /* IE11+ supports png */ ?>">
		<link rel="apple-touch-icon" href="<?php print_unescaped(image_path($_['appid'], 'favicon-touch.png')); ?>">
		<link rel="apple-touch-icon-precomposed" href="<?php print_unescaped(image_path($_['appid'], 'favicon-touch.png')); ?>">
		<link rel="mask-icon" sizes="any" href="<?php print_unescaped(image_path($_['appid'], 'favicon-mask.svg')); ?>" color="<?php p($theme->getColorPrimary()); ?>">
		<link rel="manifest" href="<?php print_unescaped(image_path($_['appid'], 'manifest.json')); ?>">
        <script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="/themes/digitaldemocratic/core/css/fontawesome/css/all.css">
        <link rel="stylesheet" href="/themes/digitaldemocratic/core/css/dd.css">
        <script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>" src="/themes/digitaldemocratic/core/js/navbar.js"></script>
		<?php emit_css_loading_tags($_); ?>
		<?php emit_script_loading_tags($_); ?>
		<?php print_unescaped($_['headers']); ?>
	</head>
	<body id="<?php p($_['bodyid']);?>">
	<?php include 'layout.noscript.warning.php'; ?>

		<?php foreach ($_['initialStates'] as $app => $initialState) { ?>
			<input type="hidden" id="initial-state-<?php p($app); ?>" value="<?php p(base64_encode($initialState)); ?>">
		<?php }?>

		<a href="#app-content" class="button primary skip-navigation skip-content"><?php p($l->t('Skip to main content')); ?></a>
		<a href="#app-navigation" class="button primary skip-navigation"><?php p($l->t('Skip to navigation of app')); ?></a>

		<div id="notification-container">
			<div id="notification"></div>
		</div>
		<header role="banner" id="header">
            
			<div id="navbar-logo" class="header-left">
				<a href="<?php print_unescaped(link_to('', 'index.php')); ?>"
					id="nextcloud">
					<img src="/themes/digitaldemocratic/core/img/logo.png" alt="" style="height: 60px;">
				</a>
			</div>

            <div id="navbar-menu-apps">
                <div id="menu-apps-btn">
                    <button type="button" id="dropdownMenuAppsButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg id="Menú_Apps" data-name="Menú Apps" xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26">
                            <rect id="Rectángulo_2" data-name="Rectángulo 2" width="6" height="6" fill="#262626"/>
                            <rect id="Rectángulo_1447" data-name="Rectángulo 1447" width="6" height="6" transform="translate(0 10)" fill="#262626"/>
                            <rect id="Rectángulo_1450" data-name="Rectángulo 1450" width="6" height="6" transform="translate(0 20)" fill="#262626"/>
                            <rect id="Rectángulo_1440" data-name="Rectángulo 1440" width="6" height="6" transform="translate(10)" fill="#262626"/>
                            <rect id="Rectángulo_1446" data-name="Rectángulo 1446" width="6" height="6" transform="translate(10 10)" fill="#262626"/>
                            <rect id="Rectángulo_1449" data-name="Rectángulo 1449" width="6" height="6" transform="translate(10 20)" fill="#262626"/>
                            <rect id="Rectángulo_1441" data-name="Rectángulo 1441" width="6" height="6" transform="translate(20)" fill="#262626"/>
                            <rect id="Rectángulo_1445" data-name="Rectángulo 1445" width="6" height="6" transform="translate(20 10)" fill="#262626"/>
                            <rect id="Rectángulo_1448" data-name="Rectángulo 1448" width="6" height="6" transform="translate(20 20)" fill="#262626"/>
                        </svg>
                    </button>
                    <div id="dropdownMenuApps" class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" >
                        <ul id="app-admin">
                            <li class="app roles">
                                <a href="https://sso.escola3.digitaldemocratic.net/auth/admin/" class="app-link" target="_blank">
                                    <div class="icon roles">
                                        <i class="fa fa-user-secret" aria-hidden="true"></i>
                                    </div>
                                    <div class="text">Rols</div>
                                </a>
                            </li>
                            <li class="app evaluation">
                                <a href="https://bfgh.aplicacions.ensenyament.gencat.cat/bfgh" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fas fa-edit"></i>
                                    </div>
                                    <div class="text">Avaluació</div>
                                </a>
                            </li>
                        </ul>
                        <ul id="app-apps" class="apps">
                            <li class="app cloud">
                                <a href="https://moodle.escola3.digitaldemocratic.net" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fas fa-graduation-cap"></i>
                                    </div>
                                    <div class="text">Aules</div>
                                </a>
                            </li>
<!--
                            <li class="app email">
                                <a href="/apps/mail" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="far fa-envelope"></i>
                                    </div>
                                    <div class="text">Correu</div>
                                </a>
                            </li>
                            <li class="app search">
                                <a href="http://duckduckgo.com" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </div>
                                    <div class="text">Cercar</div>
                                </a>
                            </li>
-->
                            <li class="app create-files">
                                <a href="/apps/files" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="far fa-file-word"></i>
                                    </div>
                                    <div class="text">Arxius</div>
                                </a>
                            </li>
                            <li class="app form-feedback">
                                <a href="/apps/polls" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fas fa-chart-bar"></i>
                                    </div>
                                    <div class="text">Enquestes</div>
                                </a>
                            </li>
                            <li class="app form-feedback">
                                <a href="/apps/forms" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fas fa-check-square"></i>
                                    </div>
                                    <div class="text">Formularis</div>
                                </a>
                            </li>
                            <li class="app chats">
                                <a href="/apps/spreed" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="far fa-comment-dots"></i>
                                    </div>
                                    <div class="text">Xat</div>
                                </a>
                            </li>
<!--
                            <li class="app meet-jitsi">
                                <a href="http://meet.jit.si" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fas fa-video"></i>
                                    </div>
                                    <div class="text">Reunions Jitsi</div>
                                </a>
                            </li>
                            <li class="app meet-bbb">
                                <a href="https://bbb.digitaldemocratic.net" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fas fa-video"></i>
                                    </div>
                                    <div class="text">Reunions BBB</div>
                                </a>
                            </li>
-->
                            <li class="app blogs">
                                <a href="https://wp.escola3.digitaldemocratic.net/wp-login.php?saml_sso" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fa fa-rss" aria-hidden="true"></i>
                                    </div>
                                    <div class="text">Blogs</div>
                                </a>
                            </li>
                            <li class="app contacts">
                                <a href="/apps/contacts" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                    </div>
                                    <div class="text">Contactes</div>
                                </a>
                            </li>
                            <li class="app schedule">
                                <a href="/apps/calendar" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div>
                                    <div class="text">Calendari</div>
                                </a>
                            </li>
                            <li class="app tasks">
                                <a href="/apps/tasks" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fa fa-list-ol" aria-hidden="true"></i>
                                    </div>
                                    <div class="text">Tasques</div>
                                </a>
                            </li>
<!--
                            <li class="app evaluation">
                                <a href="https://moodle.escola3.digitaldemocratic.net" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="far fa-edit"></i>
                                    </div>
                                    <div class="text">Avaluació</div>
                                </a>
                            </li>
-->
                            <li class="app photos">
                                <a href="/apps/photos" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="far fa-file-image"></i>
                                    </div>
                                    <div class="text">Fotos</div>
                                </a>
                            </li>
                            <li class="app maps">
                                <a href="/apps/maps" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    </div>
                                    <div class="text">Mapes</div>
                                </a>
                            </li>
                            <li class="app pad">
                                <a href="https://pad.escola3.digitaldemocratic.net" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="far fa-file-alt"></i>
                                    </div>
                                    <div class="text">Pads</div>
                                </a>
			    </li>
                        </ul>
                        <ul id="app-external" class="external-links">
                            <li class="app youtube">
                                <a href="https://youtube.com" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fab fa-youtube"></i>
                                    </div>
                                    <div class="text">Youtube</div>
                                </a>
                            </li>
                            <li class="app dict">
                                <a href="http://diccionari.cat" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fa fa-book" aria-hidden="true"></i>
                                    </div>
                                    <div class="text">Diccionari</div>
                                </a>
                            </li>
                            <li class="app meet-jitsi">
                                <a href="http://meet.jit.si" class="app-link" target="_blank">
                                    <div class="icon">
                                        <i class="fas fa-video"></i>
                                    </div>
                                    <div class="text">Reunions Jitsi</div>
                                </a>
                            </li>
                        </ul>                    
                    </div>
                </div>
            </div>

			<div id="navbar-nextcloud" class="header-right">      

				<div id="unified-search"></div>
				<div id="notifications"></div>
				<div id="contactsmenu">
					<div class="icon-contacts menutoggle" tabindex="0" role="button"
					aria-haspopup="true" aria-controls="contactsmenu-menu" aria-expanded="false">
						<span class="hidden-visually"><?php p($l->t('Contacts'));?></span>
					</div>
					<div id="contactsmenu-menu" class="menu"
						aria-label="<?php p($l->t('Contacts menu'));?>"></div>
				</div>
				<div id="settings">
					<div id="expand" tabindex="0" role="button" class="menutoggle"
						aria-label="<?php p($l->t('Settings'));?>"
						aria-haspopup="true" aria-controls="expanddiv" aria-expanded="false">
						<div class="avatardiv<?php if ($_['userAvatarSet']) {
				print_unescaped(' avatardiv-shown');
			} else {
				print_unescaped('" style="display: none');
			} ?>">
							<?php if ($_['userAvatarSet']): ?>
								<img alt="" width="32" height="32"
								src="<?php p(\OC::$server->getURLGenerator()->linkToRoute('core.avatar.getAvatar', ['userId' => $_['user_uid'], 'size' => 32, 'v' => $_['userAvatarVersion']]));?>"
								srcset="<?php p(\OC::$server->getURLGenerator()->linkToRoute('core.avatar.getAvatar', ['userId' => $_['user_uid'], 'size' => 64, 'v' => $_['userAvatarVersion']]));?> 2x, <?php p(\OC::$server->getURLGenerator()->linkToRoute('core.avatar.getAvatar', ['userId' => $_['user_uid'], 'size' => 128, 'v' => $_['userAvatarVersion']]));?> 4x"
								>
							<?php endif; ?>
						</div>
						<div id="expandDisplayName" class="icon-settings-white"></div>
					</div>
					<nav class="settings-menu" id="expanddiv" style="display:none;"
						aria-label="<?php p($l->t('Settings menu'));?>">
					<ul>
					<?php foreach ($_['settingsnavigation'] as $entry):?>
						<li data-id="<?php p($entry['id']); ?>">
							<a href="<?php print_unescaped($entry['href']); ?>"
								<?php if ($entry["active"]): ?> class="active"<?php endif; ?>>
								<img alt="" src="<?php print_unescaped($entry['icon'] . '?v=' . $_['versionHash']); ?>">
								<?php p($entry['name']) ?>
							</a>
						</li>
					<?php endforeach; ?>
					</ul>
					</nav>
				</div>
			</div>


		</header>

		<div id="sudo-login-background" class="hidden"></div>
		<form id="sudo-login-form" class="hidden" method="POST">
			<label>
				<?php p($l->t('This action requires you to confirm your password')); ?><br/>
				<input type="password" class="question" autocomplete="new-password" name="question" value=" <?php /* Hack against browsers ignoring autocomplete="off" */ ?>"
				placeholder="<?php p($l->t('Confirm your password')); ?>" />
			</label>
			<input class="confirm" value="<?php p($l->t('Confirm')); ?>" type="submit">
		</form>

		<div id="content" class="app-<?php p($_['appid']) ?>" role="main">
			<?php print_unescaped($_['content']); ?>
		</div>

	</body>
</html>
