#!/bin/sh
cp /var/www/html/resources/config/mimetypemapping.dist.json /var/www/html/config/mimetypemapping.json
sed -i '$d' /var/www/html/config/mimetypemapping.json
sed -i '${s/$/,/}' /var/www/html/config/mimetypemapping.json
echo $'\t"pad": ["application/x-ownpad"],\n\t"calc": ["application/x-ownpad"]\n}' >> /var/www/html/config/mimetypemapping.json
su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ files:scan --all'