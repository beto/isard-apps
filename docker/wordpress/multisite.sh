#!/bin/sh
cat <<EOT >> multisite.cfg
define( 'WP_ALLOW_MULTISITE', true );
define( 'MULTISITE', true );
define( 'SUBDOMAIN_INSTALL', false );
\$base = '/';
define( 'DOMAIN_CURRENT_SITE', '$WORDPRESS_DOMAIN_CURRENT_SITE' );
define( 'PATH_CURRENT_SITE', '/' );
define( 'SITE_ID_CURRENT_SITE', 1 );
define( 'BLOG_ID_CURRENT_SITE', 1 );
EOT

sed -i '/Happy publishing/e cat multisite.cfg' /var/www/html/wp-config.php
