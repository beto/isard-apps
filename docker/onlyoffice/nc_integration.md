# Resume

Everything seems a jwt token missconfiguration.

*ONLYOFFICE*
Set inbox, outbox and browser to true
Set "Authorization" header name the same in nextcloud
Set "string" **secret** the same at nextcloud plugin UI

*NEXTCLOUD*
Add the snippet to config.


# isard-apps-onlyoffice

root@903a734d2a84:/etc/onlyoffice/documentserver# cat local.json 
{
  "services": {
    "CoAuthoring": {
      "sql": {
        "type": "postgres",
        "dbHost": "localhost",
        "dbPort": "5432",
        "dbName": "onlyoffice",
        "dbUser": "onlyoffice",
        "dbPass": "onlyoffice"
      },
      "token": {
        "enable": {
          "request": {
            "inbox": true,
            "outbox": true
          },
          "browser": true
        },
        "inbox": {
          "header": "Authorization"
        },
        "outbox": {
          "header": "Authorization"
        }
      },
      "secret": {
        "inbox": {
          "string": "secret"
        },
        "outbox": {
          "string": "secret"
        },
        "session": {
          "string": "secret"
        }
      }
    }
  },
  "rabbitmq": {
    "url": "amqp://guest:guest@localhost"
  }
}


# isard-apps-nextcloud-app
/var/www/html/config # vi config.php

'onlyoffice' =>                                     
  array (                                       
    "jwt_secret" => "secret", 
    "jwt_header" => "Authorization"
) 