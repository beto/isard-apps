CREATE DATABASE wordpress;
CREATE USER wordpress@localhost IDENTIFIED BY 'W0rdpr3ss';
GRANT ALL PRIVILEGES ON wordpress.* TO wordpress@localhost;
FLUSH PRIVILEGES;