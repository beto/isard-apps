if [ ! -f /var/www/html/config.php ]; then
    curl --location https://moodle.org/plugins/download.php/23360/mod_jitsi_moodle310_2021020300.zip > jitsi.zip
    unzip jitsi.zip -d /var/www/html/mod/
    rm jitsi.zip

    curl --location https://moodle.org/plugins/download.php/23294/mod_bigbluebuttonbn_moodle310_2019101004.zip > bbb.zip
    unzip bbb.zip -d /var/www/html/mod/
    rm bbb.zip

    #ADD plugins/auth_saml2.zip /var/www/html/
    curl --location https://github.com/isard-vdi/moodle-auth_saml2/archive/refs/heads/role_map.zip > auth_saml2.zip
    unzip auth_saml2.zip -d /var/www/html/auth/
    mv /var/www/html/auth/moodle-auth_saml2-role_map /var/www/html/auth/saml2
    rm auth_saml2.zip

    curl --location https://github.com/3iPunt/moodle_mod_tresipuntshare/archive/refs/heads/master.zip > tresipuntshare.zip
    unzip tresipuntshare.zip -d /var/www/html/mod/
    mv /var/www/html/mod/moodle_mod_tresipuntshare-master /var/www/html/mod/tresipuntshare
    rm tresipuntshare.zip

    curl --location https://github.com/3iPunt/moodle_mod_tresipuntvideo/archive/refs/heads/master.zip > tresipuntvideo.zip
    unzip tresipuntvideo.zip -d /var/www/html/mod/
    mv /var/www/html/mod/moodle_mod_tresipuntvideo-master /var/www/html/mod/tresipuntvideo
    rm tresipuntvideo.zip

    curl --location https://github.com/3iPunt/moodle_mod_tresipuntaudio/archive/refs/heads/master.zip > tresipuntaudio.zip
    unzip tresipuntaudio.zip -d /var/www/html/mod/
    mv /var/www/html/mod/moodle_mod_tresipuntaudio-master /var/www/html/mod/tresipuntaudio
    rm tresipuntaudio.zip

    curl --location https://github.com/3iPunt/moodle_mod_tresipuntaudio/archive/refs/heads/master.zip > tresipuntaudio.zip
    unzip tresipuntaudio.zip -d /var/www/html/mod/
    mv /var/www/html/mod/moodle_mod_tresipuntaudio-master /var/www/html/mod/tresipuntaudio
    rm tresipuntaudio.zip

    curl --location https://github.com/3iPunt/moodle_theme_cbe/archive/refs/heads/master.zip > tresipunt_theme_cbe.zip
    unzip tresipunt_theme_cbe.zip -d /var/www/html/theme/
    mv /var/www/html/theme/moodle_theme_cbe-master /var/www/html/theme/cbe
    rm tresipunt_theme_cbe.zip
fi
chown -R nobody:root /var/www/html