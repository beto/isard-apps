# Isard Office

This repository offers a basic package of applications to be used mainly in education. It has some theme and plugin personalization and aims to be as simple to be set up as possible. Taking into account that the set up allows for SSO with SAML2 (https://gitlab.com/isard/isard-sso) you should have some technical Linux knowledge.

## Features

- Applications:
  - Moodle
  - Nextcloud
  - Wordpress
  - Etherpad
  - BigBlueButton
  - Jitsi
- Single Sign-On.
  - Integrates with https://gitlab.com/isard/isard-sso to offer SAML2 integration
  - Role and groups integration


## Nextcloud

Nexcloud is configured to use isard-openid to login. To login without isard-openid use `/login?noredir=1`.

## Moodle. Plugin development

Edit docker-compose.moodle.yml and add volumes:

    - ${DATA_FOLDER}/moodle/html:/var/www/html
    #- ${DATA_FOLDER}/moodle/html:/htmlcopy:rw

Start moodle and enter docker with root username: docker exec -ti --user root moodle /bin/sh

copy all from /var/www/html to /htmlcopy.

Stop moodle docker and modify volumes:

    #- ${DATA_FOLDER}/moodle/html:/var/www/html
    - ${DATA_FOLDER}/moodle/html:/htmlcopy:rw

Start moodle again and the code is bind inside.

Use genziplugin.sh to generate new zip with plugin code and start moodle again:

(remove /opt/isard-office and start again. It will install plugin from scratch)
Start from scratch only needed if strings are modified.

# SAML

Activate saml2 module. 
- URL is the keycloak realm saml2 url link
- Import SP metadata from moodle to create new client in keycloak

