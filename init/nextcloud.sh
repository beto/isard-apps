echo "Installing plugins..."
docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install user_saml'"
while  $? == 255 ; do 
    printf '.' ;
    sleep .5 ;
    docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install user_saml'"
done

#docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install user_saml'"