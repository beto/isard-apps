#!make
include main.conf
export $(shell sed 's/=.*//' main.conf)

VERSION := 0.0.1-rc0
export VERSION

BUILD_ROOT_PATH=$(shell pwd)

all: environment moodle nextcloud 
	cp .env docker/postgresql
	cp .env docker/mariadb
	cp .env docker/moodle
	cp .env docker/nextcloud
	cp .env docker/wordpress
	cp .env docker/etherpad
	docker-compose 	-f docker/moodle/moodle.yml \
					-f docker/nextcloud/nextcloud.yml \
					-f docker/wordpress/wordpress.yml \
					-f docker/etherpad/etherpad.yml \
					-f docker/onlyoffice/onlyoffice.yml \
					-f docker/redis/redis.yml \
					-f docker/postgresql/postgresql.yml \
					-f docker/mariadb/mariadb.yml \
					-f docker/network.yml \
					config > docker-compose.yml

start: all
	docker-compose up -d --no-deps

add-plugins:
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install mail'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:enable mail'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install user_saml'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:enable user_saml'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:install ownpad'"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "/ownpad_cfg.sh"
	docker exec -ti isard-apps-nextcloud-app /bin/sh -c "su - www-data -s /bin/sh -c 'PHP_MEMORY_LIMIT=512M php /var/www/html/occ app:enable ownpad'"


	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings app:install onlyoffice
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set onlyoffice DocumentServerUrl --value="https://oof.$$DOMAIN"
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set onlyoffice jwt_secret --value="secret"
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set onlyoffice jwt_header --value="Authorization"
	docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:system:set allow_local_remote_servers  --value=true

cleanup:
	docker-compose down
	rm -rf /opt/isard-apps


environment:
	cp main.conf .env
	echo "BUILD_ROOT_PATH=$(BUILD_ROOT_PATH)" >> .env

moodle: environment
	cp .env docker/moodle
	docker-compose 	-f docker/moodle/moodle.yml \
					-f docker/postgresql/postgresql.yml \
					-f docker/network.yml \
					config > docker-compose.moodle.yml

nextcloud: environment
	cp .env docker/nextcloud
	cp .env docker/redis
	docker-compose 	-f docker/nextcloud/nextcloud.yml \
					-f docker/postgresql/postgresql.yml \
					-f docker/redis/redis.yml \
					-f docker/network.yml \
					config > docker-compose.nextcloud.yml

wordpress: environment
	cp .env docker/wordpress
	docker-compose 	-f docker/wordpress/wordpress.yml \
					-f docker/mariadb/mariadb.yml \
					-f docker/network.yml \
					config > docker-compose.wordpress.yml

init-dbs: environment
	MOODLE="\set AUTOCOMMIT on\n; \
			CREATE USER $(MOODLE_POSTGRES_USER) SUPERUSER PASSWORD '$(MOODLE_POSTGRES_PASSWORD)'; \
			CREATE DATABASE moodle; \
			GRANT ALL PRIVILEGES ON DATABASE moodle TO $(MOODLE_POSTGRES_USER);"
	docker-compose exec isard-apps-postgresql psql -v ON_ERROR_STOP=1 \
						--username $(POSTGRES_USER) <<-EOSQL $(MOODLE) EOSQL

remove-moodle-db: environment
	docker-compose exec isard-apps-postgresql psql -v ON_ERROR_STOP=1 \
						--username $(POSTGRES_USER) -c "\set AUTOCOMMIT on\n; \
						DROP DATABASE moodle;"

reset-nextcloud: environment
	docker-compose stop isard-apps-nextcloud-nginx isard-apps-nextcloud-app
	rm -rf /opt/isard-office/nextcloud
	docker-compose exec isard-apps-postgresql psql -v ON_ERROR_STOP=1 \
						--username $(POSTGRES_USER) -c "\set AUTOCOMMIT on\n; \
						DROP DATABASE nextcloud; \
						CREATE USER $(NEXTCLOUD_POSTGRES_USER) SUPERUSER PASSWORD '$(NEXTCLOUD_POSTGRES_PASSWORD)'; \
			CREATE DATABASE nextcloud; \
			GRANT ALL PRIVILEGES ON DATABASE nextcloud TO $(NEXTCLOUD_POSTGRES_USER);"
	docker-compose up -d isard-apps-nextcloud-nginx isard-apps-nextcloud-app

backup-databases:
	echo "Todo"

restore-databases:
	echo "Todo"

db-snapshot-create: environment
	docker exec -t isard-apps-postgresql pg_dumpall -c -U admin > backups/db-snapshot.sql
	#docker exec -t isard-apps-postgresql pg_dumpall -c -U admin | gzip > backups/db-snapshot.sql.gz
	echo $$DOMAIN
	sed -i 's/$(DOMAIN)/DDOOMMAAIINN/g' backups/db-snapshot.sql

db-snapshot-restore: environment
	echo "Old domain"
	cp .env docker/postgresql
	cp .env docker/mariadb
	docker-compose 	-f docker/postgresql/postgresql.yml \
			-f docker/mariadb/mariadb.yml \
			-f docker/network.yml \
			config > databases.yml
	echo "Starting system databases..."
	docker-compose -f databases.yml up -d --no-deps

	echo "Substitute DOMAIN with: $$DOMAIN"
	cp backups/db-snapshot.sql backups/db-snapshot_$$DOMAIN.sql
	sed -i 's/DDOOMMAAIINN/$(DOMAIN)/g' backups/db-snapshot_$$DOMAIN.sql

	echo "Generating new private/public certificates for SAML..."
	openssl req  -nodes -new -x509  -keyout backups/certs/private.key -out backups/certs/public.cert

	echo "Substitute CERTIFICATES with new ones'
	grep -rl oldtext . | xargs sed -i 's/oldtext/newtext/g'

	echo "Restoring new SQL info in databases..."
	docker exec -i isard-apps-postgresql psql -U admin --set ON_ERROR_STOP=on -f backups/sample.sql

	echo "Cleaning up"
	rm databases.yml